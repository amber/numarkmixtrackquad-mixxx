// Based on Mixxx default controller settings for
// Numark Mixtrack Mapping and Numark Mixtrack Pro Script Functions
//
// 1/11/2010 - v0.1 - Matteo <matteo@magm3.com>
// 5/18/2011 - Changed by James Ralston
// 05/26/2012 to 06/27/2012 - Changed by Darío José Freije <dario2004@gmail.com>
// 30/10/2014  Einar Alex - einar@gmail.com
// 01/01/2022 Safia Ambre Kihal Cochennec - <ambre.kihal@protonmail.com>
//

// **** MIXXX v2.3.1 ****
// Known Bugs:
//	Each slide/knob needs to be moved on Mixxx startup to match levels with the Mixxx UI
//
//  What should be working.
//
//	2 Channels only
// 	---------------
//	Library Browse knob + Load A/B
//	Channel volume, cross fader, cue gain / mix, Master gain, filters, pitch and pitch bend
// 	JogWheel 													(Only standard LED)
//  Scratch/CD mode toggle (kinda)
//	Headphone output toggle
//	Samples
//	Cue
//			1-3. Hot cue
//			4. Deletes a hot cue which is pressed next
//	Loops
//			1. Loop in
//			2. Loop out
//			3. Re-loop
//			4. Loop halves / Loop doubles
//	Effects
//  Samples
// Sync
//	LED

// Not working and to do:
// ------------
//	Stutter
//  Shift + Sync
//  Shift + pads
// 	4 decks?
//	Clean up code, some features are already available.

// 	Pitch is inverted, up(-) is +bpm and down(+) is -bpm
// 	-> Preferences > Interface > Pitch/Rate slider direction > Down increases speed

function NumarkMixTrackQuad() {}

NumarkMixTrackQuad.init = function (id) {
  // called when the MIDI device is opened & set up
  NumarkMixTrackQuad.id = id; // Store the ID of this device for later use

  NumarkMixTrackQuad.directoryMode = false;
  NumarkMixTrackQuad.scratchMode = [false, false];
  NumarkMixTrackQuad.manualLoop = [true, true];
  NumarkMixTrackQuad.deleteKey = [false, false];
  NumarkMixTrackQuad.isKeyLocked = [0, 0];
  NumarkMixTrackQuad.touch = [false, false];
  NumarkMixTrackQuad.scratchTimer = [-1, -1];
  NumarkMixTrackQuad.shift = Boolean(false);
  NumarkMixTrackQuad.loopMode = Boolean(true);
  NumarkMixTrackQuad.reloopConnectionChannel1 = engine.makeConnection(
    '[Channel1]',
    'loop_enabled',
    NumarkMixTrackQuad.reloopButtonOutputCallback
  );
  NumarkMixTrackQuad.reloopConnectionChannel2 = engine.makeConnection(
    '[Channel2]',
    'loop_enabled',
    NumarkMixTrackQuad.reloopButtonOutputCallback
  );

  NumarkMixTrackQuad.midiControls = [
    // Common
    { directory: 0x4b, file: 0x4c, pitch_led: 0x0d }, // Deck 1
    {
      rate: 0x70,
      scratchMode: 0x48,
      manualLoop: 0x61,
      loop_start_position: 0x53,
      loop_end_position: 0x54,
      reloop_exit: 0x55,
      loop_size: 0x63,
      deleteKey: 0x59,
      hotCue1: 0x6d,
      hotCue2: 0x6e,
      hotCue3: 0x6f,
      stutter: 0x4a,
      Cue: 0x33,
      sync: 0x40,
      mix: 0x5c,
      fx1: 0x59,
      fx2: 0x5a,
      fx3: 0x5b,
      reset: 0x5c,
      sample_1: 0x65,
      sample_2: 0x66,
      sample_3: 0x67,
      sample_4: 0x68,
    }, // Deck 2
    {
      rate: 0x71,
      scratchMode: 0x48,
      manualLoop: 0x62,
      loop_start_position: 0x56,
      loop_end_position: 0x57,
      reloop_exit: 0x55,
      loop_size: 0x63,
      deleteKey: 0x5d,
      hotCue1: 0x6d,
      hotCue2: 0x6e,
      hotCue3: 0x6f,
      stutter: 0x4a,
      Cue: 0x33,
      sync: 0x47,
      fx1: 0x59,
      fx2: 0x5a,
      fx3: 0x5b,
      reset: 0x5c,
    },
  ];

  NumarkMixTrackQuad.ledsColors = {
    off: 0x00,
    red: 0x01,
    orange: 0x02,
    lighterOrange: 0x03,
    yellow: 0x04,
    green: 0x05,
    tealGreen: 0x06,
    tealBlue: 0x07,
    blue: 0x08,
    deepBlue: 0x09,
    purple: 0x0a,
    pink: 0x0b,
    redPink: 0x0c,
    white: 0x0d,
    lightGreen: 0x0e,
    lightPink: 0x0f,
    grey: 0x10,
  };

  NumarkMixTrackQuad.topRowPadsMidiMap = {
    89: 4,
    90: 8,
    91: 16,
    92: 32,
  };

  NumarkMixTrackQuad.ledDefault = NumarkMixTrackQuad.ledsColors['redPink'];
  NumarkMixTrackQuad.ledOn = NumarkMixTrackQuad.ledsColors['tealGreen'];
  NumarkMixTrackQuad.ledSample = NumarkMixTrackQuad.ledsColors['purple'];

  NumarkMixTrackQuad.ledSampleTimers = {
    0: 0,
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0,
    6: 0,
    7: 0,
    8: 0,
  };

  NumarkMixTrackQuad.LedTimer = function (id, led, count, state) {
    this.id = id;
    this.led = led;
    this.count = count;
    this.state = state;
  };

  for (i = 0x30; i <= 0x73; i++) midi.sendShortMsg(0x90, i, 0x00); // Turn off all the lights

  NumarkMixTrackQuad.hotCue = {
    //Deck 1
    0x6d: '1',
    0x6e: '2',
    0x6f: '3', //Deck 2
    0x6d: '1',
    0x6e: '2',
    0x6f: '3',
  };
  NumarkMixTrackQuad.turnOnSimpleLed(
    NumarkMixTrackQuad.midiControls[0]['file'],
    true
  );
  NumarkMixTrackQuad.initLedStart();

  // Enable soft-takeover for Pitch slider

  engine.softTakeover('[Channel1]', 'rate', true);
  engine.softTakeover('[Channel2]', 'rate', true);
};

NumarkMixTrackQuad.initLedStart = function () {
  //All pads while in loop Mode except for loop_size pad
  for (i = 0x59; i <= 0x5b; i++) {
    midi.sendShortMsg(0x91, i, NumarkMixTrackQuad.ledDefault);
    midi.sendShortMsg(0x92, i, NumarkMixTrackQuad.ledDefault);
  }
  midi.sendShortMsg(0x91, 0x5c, NumarkMixTrackQuad.ledDefault);
  midi.sendShortMsg(0x92, 0x5c, NumarkMixTrackQuad.ledDefault);

  for (i = 0x53; i <= 0x55; i++) {
    midi.sendShortMsg(0x91, i, NumarkMixTrackQuad.ledDefault);
    midi.sendShortMsg(0x92, i, NumarkMixTrackQuad.ledDefault);
  }
  //Loop Size Pad
  midi.sendShortMsg(
    0x91,
    NumarkMixTrackQuad.midiControls[1]['loop_size'],
    NumarkMixTrackQuad.ledDefault
  );
  midi.sendShortMsg(
    0x92,
    NumarkMixTrackQuad.midiControls[1]['loop_size'],
    NumarkMixTrackQuad.ledDefault
  );

  //Sampler mode pads
  for (i = 0x65; i <= 0x68; i++) {
    midi.sendShortMsg(0x91, i, NumarkMixTrackQuad.ledSample);
    midi.sendShortMsg(0x92, i, NumarkMixTrackQuad.ledSample);
  }
};

NumarkMixTrackQuad.turnOnSimpleLed = function (control, status) {
  status = status ? 0x7f : 0x00;
  midi.sendShortMsg(0x90, control, status);
};

NumarkMixTrackQuad.setLed = function (status, control, on) {
  if (!on) {
    midi.sendShortMsg(status, control, NumarkMixTrackQuad.ledDefault);
  } else {
    midi.sendShortMsg(status, control, NumarkMixTrackQuad.ledOn);
  }
};

NumarkMixTrackQuad.shortLed = function (channel, control, value, status) {
  if (value === 127) {
    midi.sendShortMsg(status, control, NumarkMixTrackQuad.ledOn);
  } else {
    midi.sendShortMsg(status, control, NumarkMixTrackQuad.ledDefault);
  }
};

NumarkMixTrackQuad.groupToDeck = function (group) {
  var matches = group.match(/^\[Channel(\d+)\]$/);

  if (matches === null) {
    return -1;
  } else {
    return matches[1];
  }
};

//Track selection
NumarkMixTrackQuad.selectKnob = function (
  channel,
  control,
  value,
  status,
  group
) {
  if (value > 63) {
    value = value - 128;
  }
  if (NumarkMixTrackQuad.directoryMode) {
    if (value > 0) {
      for (var i = 0; i < value; i++) {
        engine.setValue(group, 'SelectNextPlaylist', 1);
      }
    } else {
      for (var i = 0; i < -value; i++) {
        engine.setValue(group, 'SelectPrevPlaylist', 1);
      }
    }
  } else {
    engine.setValue(group, 'SelectTrackKnob', value);
  }
};

NumarkMixTrackQuad.toggleDirectoryMode = function (
  channel,
  control,
  value,
  status,
  group
) {
  // Toggle setting and light
  if (value) {
    NumarkMixTrackQuad.directoryMode = !NumarkMixTrackQuad.directoryMode;

    NumarkMixTrackQuad.turnOnSimpleLed(
      NumarkMixTrackQuad.midiControls[0]['directory'],
      NumarkMixTrackQuad.directoryMode
    );
    NumarkMixTrackQuad.turnOnSimpleLed(
      NumarkMixTrackQuad.midiControls[0]['file'],
      !NumarkMixTrackQuad.directoryMode
    );
  }
};

NumarkMixTrackQuad.cuebutton = function (
  channel,
  control,
  value,
  status,
  group
) {
  // Don't set Cue accidentaly at the end of the song
  if (engine.getValue(group, 'playposition') <= 0.97) {
    engine.setValue(group, 'cue_default', value ? 1 : 0);
  } else {
    engine.setValue(group, 'cue_preview', value ? 1 : 0);
  }
};

// Stutters adjust BeatGrid
NumarkMixTrackQuad.playFromCue = function (
  channel,
  control,
  value,
  status,
  group
) {
  var deck = NumarkMixTrackQuad.groupToDeck(group);

  if (engine.getValue(group, 'beats_translate_curpos')) {
    engine.setValue(group, 'beats_translate_curpos', 0);
    //NumarkMixTrackQuad.setLED(NumarkMixTrackQuad.leds[deck]["stutter"], 0);
  } else {
    engine.setValue(group, 'beats_translate_curpos', 1);
    //NumarkMixTrackQuad.setLED(NumarkMixTrackQuad.leds[deck]["stutter"], 1);
  }
};

NumarkMixTrackQuad.changeHotCue = function (
  channel,
  control,
  value,
  status,
  group
) {
  var deck = NumarkMixTrackQuad.groupToDeck(group);
  var hotCue = NumarkMixTrackQuad.hotCue[control];

  // onHotCueChange called automatically
  if (NumarkMixTrackQuad.deleteKey[deck - 1]) {
    if (engine.getValue(group, 'hotcue_' + hotCue + '_enabled')) {
      engine.setValue(group, 'hotcue_' + hotCue + '_clear', 1);
    }
    NumarkMixTrackQuad.toggleDeleteKey(channel, control, value, status, group);
  } else {
    if (value) {
      engine.setValue(group, 'hotcue_' + hotCue + '_activate', 1);
    } else {
      engine.setValue(group, 'hotcue_' + hotCue + '_activate', 0);
    }
  }
};

NumarkMixTrackQuad.playbutton = function (
  channel,
  control,
  value,
  status,
  group
) {
  if (!value) return;

  var deck = NumarkMixTrackQuad.groupToDeck(group);

  if (engine.getValue(group, 'play')) {
    engine.setValue(group, 'play', 0);
  } else {
    engine.setValue(group, 'play', 1);
  }
};

//LOOPS
NumarkMixTrackQuad.reloopButtonOutputCallback = function (value, channel) {
  if (value) {
    if (channel === '[Channel1]') {
      midi.sendShortMsg(
        0x91,
        NumarkMixTrackQuad.midiControls[1]['reloop_exit'],
        NumarkMixTrackQuad.ledOn
      );
    } else if (channel === '[Channel2]') {
      midi.sendShortMsg(
        0x92,
        NumarkMixTrackQuad.midiControls[1]['reloop_exit'],
        NumarkMixTrackQuad.ledOn
      );
    }
  } else {
    if (channel === '[Channel1]') {
      midi.sendShortMsg(
        0x91,
        NumarkMixTrackQuad.midiControls[1]['reloop_exit'],
        NumarkMixTrackQuad.ledDefault
      );
    } else if (channel === '[Channel2]') {
      midi.sendShortMsg(
        0x92,
        NumarkMixTrackQuad.midiControls[1]['reloop_exit'],
        NumarkMixTrackQuad.ledDefault
      );
    }
  }
};

NumarkMixTrackQuad.loopSize = function (
  channel,
  control,
  value,
  status,
  group
) {
  if (value === 0x7f) {
    if (!NumarkMixTrackQuad.shift) {
      engine.setValue(group, 'loop_halve', 1);
    } else if (NumarkMixTrackQuad.shift) {
      engine.setValue(group, 'loop_double', 1);
    }
  }
};

//BPM
NumarkMixTrackQuad.pitch = function (channel, control, value, status, group) {
  var deck = NumarkMixTrackQuad.groupToDeck(group);

  var pitch_value = 0;

  if (value < 64) pitch_value = (value - 64) / 64;
  if (value > 64) pitch_value = (value - 63) / 64;

  engine.setValue('[Channel' + deck + ']', 'rate', pitch_value);
};

NumarkMixTrackQuad.beatsync = function (
  channel,
  control,
  value,
  status,
  group
) {
  var deck = NumarkMixTrackQuad.groupToDeck(group);

  if (NumarkMixTrackQuad.deleteKey[deck - 1]) {
    // Delete + SYNC = vuelve pitch a 0
    engine.softTakeover(group, 'rate', false);
    engine.setValue(group, 'rate', 0);
    engine.softTakeover(group, 'rate', true);

    NumarkMixTrackQuad.toggleDeleteKey(channel, control, value, status, group);
  } else {
    if (deck === 1) {
      if (!engine.getValue('[Channel2]', 'play')) {
        engine.setValue(group, 'beatsync_tempo', value ? 1 : 0);
      } else {
        engine.setValue(group, 'beatsync', value ? 1 : 0);
      }
    }

    if (deck === 2) {
      if (!engine.getValue('[Channel1]', 'play')) {
        engine.setValue(group, 'beatsync_tempo', value ? 1 : 0);
      } else {
        engine.setValue(group, 'beatsync', value ? 1 : 0);
      }
    }
  }
};

NumarkMixTrackQuad.reloop = function (channel, control, value, status, group) {
  var loop_start = engine.getValue(group, 'loop_start_position');
  var loop_end = engine.getValue(group, 'loop_end_position');
  if (value === 127) {
    if (loop_start !== -1 && loop_end !== -1) {
      engine.setValue(group, 'reloop_toggle', true);
    } else {
      engine.setValue(group, 'loop_in_goto', true);
      engine.setValue(group, 'beatloop_activate', true);
    }
  }
};

//Effects
NumarkMixTrackQuad.topRowPads = function (
  channel,
  control,
  value,
  status,
  group
) {
  if (NumarkMixTrackQuad.shift) {
    var beat_loop_size =
      'beatloop_' +
      NumarkMixTrackQuad.topRowPadsMidiMap[control.toString()] +
      '_activate';
    engine.setValue('[Channel' + channel + ']', beat_loop_size, true);
  } else if (!NumarkMixTrackQuad.shift) {
    if (control === 92) {
      NumarkMixTrackQuad.mixZero(channel, control, value, status, group);
    } else {
      if (value === 127) {
        if (engine.getValue(group, 'enabled')) {
          NumarkMixTrackQuad.setLed(status, control, false);
          engine.setValue(group, 'enabled', false);
        } else {
          NumarkMixTrackQuad.setLed(status, control, true);
          engine.setValue(group, 'enabled', true);
        }
      }
    }
  }
};

NumarkMixTrackQuad.effectKnob = function (
  channel,
  control,
  value,
  status,
  group
) {
  const currentMeta = engine.getValue(group, 'meta');
  const step = 1 / 25;
  if (NumarkMixTrackQuad.shift) {
    if (value === 127) {
      engine.setValue(group, 'effect_selector', -1);
    } else if (value === 1) {
      engine.setValue(group, 'effect_selector', 1);
    }
  } else if (!NumarkMixTrackQuad.shift) {
    if (value === 1) {
      engine.setValue(group, 'meta', currentMeta + step);
    } else if (value === 127) {
      engine.setValue(group, 'meta', currentMeta - step);
    }
  }
};

NumarkMixTrackQuad.effectMix = function (
  channel,
  control,
  value,
  status,
  group
) {
  const currentMix = engine.getValue(group, 'mix');
  const step = 1 / 25;

  if (value < 64) {
    engine.setValue(group, 'mix', currentMix + step);
  } else if (value > 64) {
    engine.setValue(group, 'mix', currentMix - step);
  }
};

NumarkMixTrackQuad.mixZero = function (channel, control, value, status, group) {
  NumarkMixTrackQuad.shortLed(channel, control, value, status);
  script.triggerControl(group, 'mix_set_zero');
};

NumarkMixTrackQuad.samplePlay = function (
  channel,
  control,
  value,
  status,
  group
) {
  const sampler_number = group[8];
  if (value === 0x7f) {
    engine.setValue(group, 'cue_gotoandplay', true);
    midi.sendShortMsg(
      status,
      control,
      NumarkMixTrackQuad.ledsColors['tealGreen']
    );
    if (NumarkMixTrackQuad.ledSampleTimers[sampler_number] !== 0) {
      engine.stopTimer(NumarkMixTrackQuad.ledSampleTimers[sampler_number]);
      NumarkMixTrackQuad.ledSampleTimers[sampler_number] = 0;
    }
    const samples = engine.getValue(group, 'track_samples');
    const length = (samples / 44100) * 500;
    NumarkMixTrackQuad.ledSampleTimers[sampler_number] = engine.beginTimer(
      length,
      'NumarkMixTrackQuad.resetSampleLed(' +
        control +
        ', ' +
        status +
        ', ' +
        sampler_number +
        ')',
      true
    );
  }
};

NumarkMixTrackQuad.resetSampleLed = function (control, status, sampler_number) {
  NumarkMixTrackQuad.ledSampleTimers[sampler_number] = 0;
  midi.sendShortMsg(status, control, NumarkMixTrackQuad.ledSample);
};

//Wheels
NumarkMixTrackQuad.jogWheel = function (
  channel,
  control,
  value,
  status,
  group
) {
  var deck = NumarkMixTrackQuad.groupToDeck(group);

  var adjustedJog = parseFloat(value);
  var posNeg = 1;
  if (adjustedJog > 63) {
    // Counter-clockwise
    posNeg = -1;
    adjustedJog = value - 128;
  }

  if (engine.getValue(group, 'play')) {
    if (
      NumarkMixTrackQuad.scratchMode[deck - 1] &&
      posNeg === -1 &&
      !NumarkMixTrackQuad.touch[deck - 1]
    ) {
      if (NumarkMixTrackQuad.scratchTimer[deck - 1] != -1)
        engine.stopTimer(NumarkMixTrackQuad.scratchTimer[deck - 1]);
      NumarkMixTrackQuad.scratchTimer[deck - 1] = engine.beginTimer(
        20,
        'NumarkMixTrackQuad.jogWheelStopScratch(' + deck + ')',
        true
      );
    }
  } else {
    if (!NumarkMixTrackQuad.touch[deck - 1]) {
      if (NumarkMixTrackQuad.scratchTimer[deck - 1] != -1)
        engine.stopTimer(NumarkMixTrackQuad.scratchTimer[deck - 1]);
      NumarkMixTrackQuad.scratchTimer[deck - 1] = engine.beginTimer(
        20,
        'NumarkMixTrackQuad.jogWheelStopScratch(' + deck + ')',
        true
      );
    }
  }

  engine.scratchTick(deck, adjustedJog);

  if (engine.getValue(group, 'play')) {
    var gammaInputRange = 13; // Max jog speed
    var maxOutFraction = 0.8; // Where on the curve it should peak; 0.5 is half-way
    var sensitivity = 0.5; // Adjustment gamma
    var gammaOutputRange = 2; // Max rate change

    adjustedJog =
      posNeg *
      gammaOutputRange *
      Math.pow(
        Math.abs(adjustedJog) / (gammaInputRange * maxOutFraction),
        sensitivity
      );
    engine.setValue(group, 'jog', adjustedJog);
  }
};

NumarkMixTrackQuad.jogWheelStopScratch = function (deck) {
  NumarkMixTrackQuad.scratchTimer[deck - 1] = -1;
  engine.scratchDisable(deck);

  if (NumarkMixTrackQuad.isKeyLocked[deck - 1] === 1) {
    // Restore the previous state of the Keylock
    engine.setValue(
      '[Channel' + deck + ']',
      'keylock',
      NumarkMixTrackQuad.isKeyLocked[deck - 1]
    );
    NumarkMixTrackQuad.isKeyLocked[deck - 1] = 0;
  }
};

NumarkMixTrackQuad.wheelTouch = function (
  channel,
  control,
  value,
  status,
  group
) {
  var deck = NumarkMixTrackQuad.groupToDeck(group);

  if (!value) {
    NumarkMixTrackQuad.touch[deck - 1] = false;

    if (NumarkMixTrackQuad.scratchTimer[deck - 1] != -1)
      engine.stopTimer(NumarkMixTrackQuad.scratchTimer[deck - 1]);

    NumarkMixTrackQuad.scratchTimer[deck - 1] = engine.beginTimer(
      20,
      'NumarkMixTrackQuad.jogWheelStopScratch(' + deck + ')',
      true
    );
  } else {
    if (
      !NumarkMixTrackQuad.scratchMode[deck - 1] &&
      engine.getValue(group, 'play')
    )
      return;

    // Save the current state of the keylock
    NumarkMixTrackQuad.isKeyLocked[deck - 1] = engine.getValue(
      group,
      'keylock'
    );
    // Turn the Keylock off for scratching
    if (NumarkMixTrackQuad.isKeyLocked[deck - 1]) {
      engine.setValue(group, 'keylock', 0);
    }

    if (NumarkMixTrackQuad.scratchTimer[deck - 1] != -1)
      engine.stopTimer(NumarkMixTrackQuad.scratchTimer[deck - 1]);

    // change the 600 value for sensibility
    engine.scratchEnable(deck, 600, 33 + 1 / 3, 1.0 / 8, 1.0 / 8 / 32);

    NumarkMixTrackQuad.touch[deck - 1] = true;
  }
};

NumarkMixTrackQuad.toggleScratchMode = function (
  channel,
  control,
  value,
  status,
  group
) {
  if (!value) return;

  var deck = NumarkMixTrackQuad.groupToDeck(group);
  // Toggle setting and light
  NumarkMixTrackQuad.scratchMode[deck - 1] =
    !NumarkMixTrackQuad.scratchMode[deck - 1];
  if (NumarkMixTrackQuad.scratchMode[deck - 1]) {
    midi.sendShortMsg(status, control, 0x7f);
  } else {
    midi.sendShortMsg(status, control, 0x00);
  }
};

//Miscellaneous
NumarkMixTrackQuad.toggleDeleteKey = function (
  channel,
  control,
  value,
  status,
  group
) {
  if (!value) return;

  var deck = NumarkMixTrackQuad.groupToDeck(group);
  NumarkMixTrackQuad.deleteKey[deck - 1] =
    !NumarkMixTrackQuad.deleteKey[deck - 1];
  //NumarkMixTrackQuad.setLED(NumarkMixTrackQuad.leds[deck]["deleteKey"], NumarkMixTrackQuad.deleteKey[deck-1]);
};

NumarkMixTrackQuad.shiftButton = function (
  channel,
  control,
  value,
  status,
  group
) {
  NumarkMixTrackQuad.shift = !NumarkMixTrackQuad.shift;
};
